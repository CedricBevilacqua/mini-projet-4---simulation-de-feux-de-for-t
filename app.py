import random

from tkinter import * 

def InitializeAutomate(map, p):
    

    for lineBoucle in range(0,y):
        for colBoucle in range(0,x):
            randomnumber = random.randint(0, 100)
            if randomnumber <= p*100:
                map[colBoucle][lineBoucle] = 1
            else:
                map[colBoucle][lineBoucle] = 0

def UpdateCanvas(map, x, y, sizesquare):
    for lineBoucle in range(0,y):
        for colBoucle in range(0,x):
            if map[colBoucle][lineBoucle] == 0:
                canvas.create_rectangle(sizesquare*colBoucle, sizesquare*lineBoucle, sizesquare*(colBoucle+1), sizesquare*(lineBoucle+1), fill="grey")
            else:
                canvas.create_rectangle(sizesquare*colBoucle, sizesquare*lineBoucle, sizesquare*(colBoucle+1), sizesquare*(lineBoucle+1), fill="green")

def CopyMap(map1, map2):
    for lineBoucle in range(0,y):
        for colBoucle in range(0,x):
            map2[colBoucle][lineBoucle] = map1[colBoucle][lineBoucle]

x = 20
y = 10
p = 0.6
sizesquare = 30
map = [[0 for xboucle in range(y)] for yboucle in range(x)]
mapbuffer = [[0 for xboucle in range(y)] for yboucle in range(x)]

InitializeAutomate(map, p)
CopyMap(map, mapbuffer)

frame = Tk()

canvas = Canvas(frame, width=sizesquare*x, height=sizesquare*y)
canvas.pack()

UpdateCanvas(map, x, y, sizesquare)

frame.mainloop()